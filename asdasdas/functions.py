def hello(name, fromWhom = 'someone'):
    return 'hello ' + name + ' from ' + fromWhom
    
def testDynamic(paramShwitch):
    if paramShwitch == 'n':
        return 5
    else:
        return 'some text'
    
#print(hello('andrei', fromWhom = 'someone else'))
print(type(testDynamic('n')))
print(type(testDynamic('s')))
